package consumer;

import java.util.HashMap;
import java.util.Map;

import kafka.serializer.StringDecoder;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.streaming.kafka.KafkaUtils;
import org.apache.spark.streaming.kafka.OffsetRange;

import scala.Tuple2;

public class Main {


	private static JavaSparkContext sc; 
	private static Map<String, String> kafkaParams;
	private static long timestamp = 1510670880247L;
    private static int scale = 30000;
    private static JavaPairRDD<String, String> rdd;
	
	public static void main(String[] args) {
		if (args.length < 2){
			System.out.println("No arguments are passed.");
			 return;
		}
        SparkConf conf = new SparkConf()
                .setAppName("kafka-sandbox")
                .setMaster("local[*]");
        sc = new JavaSparkContext(conf);
        kafkaParams = new HashMap<String, String>();
        kafkaParams.put("metadata.broker.list", "quickstart.cloudera:9092");
        
        timestamp = 1510670880247L;
        scale = 30000;
        int offsetStart = Integer.parseInt(args[0]);
        int offsetEnd = Integer.parseInt(args[1]);
        OffsetRange[] offsetRanges = {
        		  OffsetRange.create("mytopic", 0, offsetStart, offsetEnd),
        		  OffsetRange.create("mytopic", 1, offsetStart, offsetEnd),
        		  OffsetRange.create("mytopic", 2, offsetStart, offsetEnd),
        		  OffsetRange.create("mytopic", 3, offsetStart, offsetEnd),
        		  OffsetRange.create("mytopic", 4, offsetStart, offsetEnd)
        };
       
        rdd = KafkaUtils.createRDD(
        		sc,
                String.class,
                String.class,
                StringDecoder.class,
                StringDecoder.class,
                kafkaParams,
                offsetRanges
        );
        JavaRDD<String> outputRDD = run(rdd);
        outputRDD.foreach(new VoidFunction<String>(){
			private static final long serialVersionUID = 1L;

			@Override
			public void call(String arg0) throws Exception {
				System.out.println(arg0);
			}
        	
        });
	}
	
	public static JavaRDD<String> run(JavaPairRDD<String, String> rdd){
        
        JavaPairRDD<Tuple2<Integer, Long>, Integer[]> rdd1 = rdd.mapToPair(new PairFunction<Tuple2<String,String>,Tuple2<Integer, Long>, Integer[]>(){
		
			private static final long serialVersionUID = 1L;
			@Override
			public Tuple2<Tuple2<Integer,Long>, Integer[]> call(Tuple2<String, String> tuple)
					throws Exception {
				String [] s = tuple._2.split(",");
				Long id = (Long.parseLong(s[1]) - timestamp)/scale*scale + timestamp;
				Integer[] values = new Integer[2];
				values[0] = Integer.parseInt(s[2]);
				values[1] = 1;
				return new Tuple2<Tuple2<Integer,Long>, Integer[]>(new Tuple2<Integer,Long>(Integer.parseInt(s[0]), id), values);
				}
        });

        rdd1 = rdd1.reduceByKey(new Function2<Integer[], Integer[], Integer[]>(){
			
			private static final long serialVersionUID = 1L;

			@Override
			public Integer[] call(Integer[] accum, Integer[] value) throws Exception {
				accum[0] += value[0];
				accum[1] += value[1];
				return accum;
			}
        	
        });        
        
        JavaRDD<String> outputRDD = rdd1.map(new Function<Tuple2<Tuple2<Integer,Long>,Integer[]>,String>(){
			private static final long serialVersionUID = 1L;

			@Override
			public String call(Tuple2<Tuple2<Integer, Long>, Integer[]> value)
					throws Exception {
				value._2[0] /= value._2[1];
				return value._1._1 + "," + value._1._2 + ",30s," + value._2[0];
			}
        });
        return outputRDD;
	}

}
