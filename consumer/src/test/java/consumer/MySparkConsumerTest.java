package consumer;

import java.util.Arrays;
import java.util.List;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.junit.Test;
import scala.Tuple2;
import com.holdenkarau.spark.testing.JavaRDDComparisons;
import com.holdenkarau.spark.testing.SharedJavaSparkContext;


public class MySparkConsumerTest extends SharedJavaSparkContext { 
	
	@Test
	public void verifyMapReduceTest() {
	    List<Tuple2<String, String>> input = Arrays.asList(new Tuple2<String, String>("1","5,1510670916280,13"), new Tuple2<String, String>("2","7,1510670916283,49"));
	    JavaPairRDD<String, String> inputRDD = jsc().parallelizePairs(input);
	    JavaRDD<String> result = Main.run(inputRDD);
	    List<String> expectedOutput = Arrays.asList("5,1510670910247,30s,13", "7,1510670910247,30s,49");
	    JavaRDD<String> expectedRDD = jsc().parallelize(expectedOutput);
	    JavaRDDComparisons.assertRDDEquals(result, expectedRDD);
	  }
}
