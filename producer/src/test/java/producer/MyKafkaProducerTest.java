package producer;

import producer.MyKafkaProducer;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.producer.MockProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MyKafkaProducerTest {
	
	 private MockProducer<String, String> producer;
	 private String topic = "test";
	 private String inputFile = "/home/cloudera/testinput.txt";

	 @Before
	 public void setUp() {
		 producer = new MockProducer<String, String>(true, new StringSerializer(), new StringSerializer());
	 }
	 
	 
	 @Test
	 public void testProducer() throws IOException, InterruptedException, ExecutionException {
		 MyKafkaProducer myProducer = new MyKafkaProducer();
		 myProducer.producer = producer;
	     myProducer.topic = topic;
	     myProducer.inputFile = inputFile;
	     myProducer.sendMessages();
	     List<ProducerRecord<String, String>> history = producer.history();
	     List<ProducerRecord<String, String>> expected = Arrays.asList(
	    		 new ProducerRecord<String, String>(topic, "5,1510670916280,13"),
	    		 new ProducerRecord<String, String>(topic, "7,1510670916283,49"),
	    		 new ProducerRecord<String, String>(topic, "1,1510670916286,48"));
	     Assert.assertEquals("Sent messages not and expected not equals", expected, history);
	 }

}
