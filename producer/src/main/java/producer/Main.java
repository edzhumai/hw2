package producer;

import java.util.Properties;

public class Main {
	
	 public static void main(String[] args) throws Exception {

		 if (args.length < 1){
				System.out.println("Please pass input file.");
			 return;
		 }
		 
		 String topic = "mytopic";
		 Properties props = new Properties();
		 props.put("bootstrap.servers", "quickstart.cloudera:9092");
		 props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		 props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
	     MyKafkaProducer logProducer = new MyKafkaProducer(props, topic, args[0]);
	     logProducer.sendMessages();
	    }
}
