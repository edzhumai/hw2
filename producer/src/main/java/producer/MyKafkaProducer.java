package producer;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

public class MyKafkaProducer
{
    Producer<String, String> producer;
    String topic;
    String inputFile;

    /**
     * KafkaProducer constructor with parameters
     * @param param   		Kafka params
     * @param topic         Kafka topic
     * @param inputFile   	Input file path
     */
    public MyKafkaProducer(Properties params, String topic, String inputFile) {
        producer = new KafkaProducer<String, String>(params);
        this.topic = topic;
        this.inputFile = inputFile;
    }
    
    /**
     * KafkaProducer default constructor
     */
    public MyKafkaProducer() { }

    
    /**
     * Reading input file and send messages to consumer
     * @throws ExecutionException 
     * @throws InterruptedException 
     */
    public void sendMessages() throws InterruptedException, ExecutionException{
        long offsetStart = -1;
        long offsetEnd = -1;
        try(BufferedReader br = new BufferedReader(new FileReader(inputFile))) {
            String line = br.readLine();
            Future<RecordMetadata> recordMetadata = null;
            while (line != null) {
            	ProducerRecord<String, String> record = new ProducerRecord<>(topic, line);
            	recordMetadata = producer.send(record);
            	if (offsetStart == -1){
                   	offsetStart = recordMetadata.get().offset();
            	}
                line = br.readLine();
            }
            if (recordMetadata != null){
            	offsetEnd = recordMetadata.get().offset();
            }
        } catch (FileNotFoundException e) {
			System.out.println("Input file not found.");
		} catch (IOException e) {
			System.out.println("Input/Output Error while reading input file.");
		}
        System.out.println("Offset start: " + offsetStart);
        System.out.println("Offset end: " + offsetEnd);
        producer.close();
    }

}